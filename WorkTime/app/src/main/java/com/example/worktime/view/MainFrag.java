package com.example.worktime.view;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.worktime.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


public final class MainFrag extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstState) {
        View view = null;
        try {
            super.onCreateView(inflater, container, savedInstState);

            // Load the xml file that corresponds to this Java file.
            view = inflater.inflate(R.layout.frag_main, container, false);
            FloatingActionButton fab = view.findViewById(R.id.floatingActionButton);
            fab.setOnClickListener(new addProject());
            RecyclerView recycler = view.findViewById(R.id.recycler);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
            recycler.setLayoutManager(layoutManager);
            MainActivity act = (MainActivity)getActivity();
            recycler.setAdapter(new ProjectAdapter(act));

            // Get the floating action button and add a function to it.
        } catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
        return view;
    }

   private final class addProject implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            FragmentActivity act = getActivity();
            Fragment frag = new AddProjectFrag();
            FragmentTransaction trans = act.getSupportFragmentManager().beginTransaction();
            trans.replace(R.id.fragContainer,frag);
            trans.addToBackStack(null);
            trans.commit();


        }
    }
}
