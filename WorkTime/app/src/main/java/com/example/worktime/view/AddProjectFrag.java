package com.example.worktime.view;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.worktime.R;
import com.example.worktime.model.AppDatabase;
import com.example.worktime.model.Project;
import com.example.worktime.model.ProjectDAO;

public class AddProjectFrag extends  androidx.fragment.app.Fragment{
    private EditText titleView;
    private EditText disciptionView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstState) {
        View view = null;
        try {
            super.onCreateView(inflater, container, savedInstState);

            // Load the xml file that corresponds to this Java file.
            view = inflater.inflate(R.layout.frag_add_project, container, false);

            Button btnAdd = view.findViewById(R.id.buttonAdd);
            Button btnCan = view.findViewById(R.id.buttonCancel);
            btnCan.setOnClickListener(new cancelProject());
            btnAdd.setOnClickListener(new addProject());
            // Get the floating action button and add a function to it.
            titleView = view.findViewById(R.id.editTxtTitle);
            disciptionView = view.findViewById(R.id.editTxtDiscription);


        }
        catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
        return view;
    }
    private final class addProject implements View.OnClickListener {
        @Override
        public void onClick(View v) {
        String title = titleView.getText().toString();
        String discripion = disciptionView.getText().toString();
            Project proj = new Project(title,discripion);

            Context appCtx = getContext();
            AppDatabase db = AppDatabase.getInstance(appCtx);
            ProjectDAO pdao = db.getProjectDAO();
            pdao.insert(proj);
            getActivity().onBackPressed();


        }
    }
    private final class cancelProject implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            getActivity().onBackPressed();



        }
    }


}
