package com.example.worktime.view;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;

import java.util.List;

import com.example.worktime.R;
import com.example.worktime.model.AppDatabase;
import com.example.worktime.model.Project;
import com.example.worktime.model.ProjectDAO;


final class ProjectAdapter
        extends RecyclerView.Adapter<ProjectAdapter.ViewHolder> {
    // A reference to the main activity
    private final MainActivity activity;

    // A reference to the project DAO to read and write to the Project table.
    private final ProjectDAO projDao;

    // A list of all projects stored in the Project table. This is an in
    // memory copy of the projects that are stored in the Project table
    // of the Room database.
    private final List<Project> allProjects;


    ProjectAdapter(MainActivity activity) {
        // Tell the RecyclerView that the project keys are stable.
        setHasStableIds(true);

        this.activity = activity;


        Context appCtx = activity.getApplicationContext();
        AppDatabase db = AppDatabase.getInstance(appCtx);
        projDao  = db.getProjectDAO();


        allProjects = projDao.getAll();


    }

    @Override
    public int getItemCount() {
        // stored in the in memory list of projects.
        return allProjects.size();
    }

    @Override
    public long getItemId(int index) {
        // stored in the list of projects at index.
        Project proj = allProjects.get(index);
        return proj.getProjectKey();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_project, parent, false);
        return new ViewHolder(view);
    }

    // This onBindViewHolder method will be called each time that the
    // WorkTime app displays a project in a row of the RecyclerView.
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int index) {
        try {
            holder.bind(index);
        }
        catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
    }


    // Each ViewHolder object corresponds to one row in the RecyclerView.
    // Each ViewHolder object will hold three TextViews that display a
    // project to the user. The TextViews are defined in item_project.xml.
    final class ViewHolder extends RecyclerView.ViewHolder
            implements OnLongClickListener, OnMenuItemClickListener {

        // References to the three TextViews in this row.
        private final TextView txtStartTime, txtTitle, txtTimeSpent;

        // A reference to the project object that
        // is displayed (held) in this ViewHolder.
        private Project heldProj;


        ViewHolder(View view) {
            super(view);

            // TextViews that are in the corresponding row.
            txtTitle = view.findViewById(R.id.txtProjectTitle);
            txtStartTime = view.findViewById(R.id.txtTimeStart);
            txtTimeSpent = view.findViewById(R.id.txtTotalTime);

            view.setOnLongClickListener(this);
        }

        // Bind this ViewHolder to the project that is
        // stored at index in the list of all projects.
        void bind(int index) {
            heldProj = allProjects.get(index);

            // the data that is in heldProj.
            txtTitle.setText(heldProj.getTitle());
        }

        // This onLongClick method will be called when the
        // user long presses one row in the RecyclerView.
        @Override
        public boolean onLongClick(View view) {
            try {
                // Create a popup menu and show it to the user.
                PopupMenu menu = new PopupMenu(activity, view);
                menu.getMenuInflater().inflate(R.menu.popup, menu.getMenu());
                menu.setOnMenuItemClickListener(this);
                menu.show();
            }
            catch (Exception ex) {
                Log.e(MainActivity.TAG, ex.toString());
            }
            return true;
        }

        // This onMenuItemClick method will be called when
        // the user presses an item on the popup menu.
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            boolean handled = false;
            try {
                switch (item.getItemId()) {
                    case R.id.itmDelete:
                        // pressed (heldProj) from the Project table.
                        projDao.delete(heldProj);

                        // Get the index of the project that
                        // the user long pressed (heldProj)
                        // from the in memory list of projects.
                        int index =allProjects.indexOf(heldProj);

                        // the in memory list of projects.
                        allProjects.remove(index);

                        // Notify the RecyclerView that
                        // heldProj has been deleted.
                        notifyItemRemoved(index);
                        handled = true;
                        break;
                }
            }
            catch (Exception ex) {
                Log.e(MainActivity.TAG, ex.toString());
            }
            return handled;
        }
    }
}